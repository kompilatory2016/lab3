class Node(object):
    def accept(self, visitor):
        return visitor.visit(self)

    def __init__(self):
        self.children = ()


class BinExpr(Node):
    def __init__(self, op, left, right, line):
        self.op = op
        self.left = left
        self.right = right
        self.line = line


class FunctionInvokation(Node):
    def __init__(self, id, args, line):
        self.id = id
        self.args = args
        self.line = line


class ExpressionList(Node):
    def __init__(self):
        self.expressions = list()


class Const(Node):
    def __init__(self, value, line):
        self.value = value
        self.line = line


class Integer(Const):
    pass
    # ...


class Float(Const):
    pass
    # ...


class String(Const):
    pass
    # ...


class Variable(Node):
    pass
    # ...


class Compound(Node):
    def __init__(self, declarations, instructions_opt):
        self.declarations = declarations
        self.instructions_opt = instructions_opt


class CodeBlocks(Node):
    def __init__(self):
        self.children = list()


class ChoiceInstr(Node):
    def __init__(self, condition, instruction, line, else_instruction=None):
        self.condition = condition
        self.instruction = instruction
        self.line = line
        self.else_instruction = else_instruction


class WhileInstr(Node):
    def __init__(self, condition, instruction, line):
        self.condition = condition
        self.instruction = instruction
        self.line = line


class RepeatInstr(Node):
    def __init__(self, instructions, condition, line):
        self.instructions = instructions
        self.condition = condition
        self.line = line


class Declarations(Node):
    def __init__(self):
        self.declarations = list()


class Declaration(Node):
    def __init__(self, type, inits, line):
        self.type = type
        self.inits = inits
        self.line = line


class Inits(Node):
    def __init__(self, line):
        self.line = line
        self.inits = list()


class Init(Node):
    def __init__(self, id, expression, line):
        self.id = id
        self.expression = expression
        self.line = line


class FunDefs(Node):
    def __init__(self):
        self.fundefs = list()


class FunDef(Node):
    def __init__(self, type, id, args, instructions, line):
        self.type = type
        self.id = id
        self.args = args
        self.instructions = instructions
        self.line = line


class Instructions(Node):
    def __init__(self):
        self.instructions = list()


class PrintInstruction(Node):
    def __init__(self, expr_list):
        self.expr_list = expr_list


class LabeledInstruction(Node):
    def __init__(self, id, instruction, line):
        self.id = id
        self.instruction = instruction
        self.line = line


class ReturnInstr(Node):
    def __init__(self, expression, line):
        self.expression = expression
        self.line = line


class Label(Node):
    # continue break etc.
    def __init__(self, label, line):
        self.label = label
        self.line = line


class Args(Node):
    def __init__(self):
        self.args = list()


class Arg(Node):
    def __init__(self, type, id, line):
        self.type = type
        self.id = id
        self.line = line


class Assignment(Node):
    def __init__(self, id, expression, line):
        self.id = id
        self.expression = expression
        self.line = line
