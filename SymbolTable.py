class LabeledInstructionSymbol:
    def __init__(self, name, instruction):
        self.name = name
        self.instruction = instruction


class VariableSymbol:
    def __init__(self, name, type):
        self.name = name
        self.type = type


class FunDefSymbol:
    def __init__(self, name, return_type, args):
        self.name = name
        self.return_type = return_type
        self.args = args


class SymbolTable(object):
    def __init__(self, parent, name):  # parent scope and symbol table name
        self.parent = parent
        self.name = name
        self.symbols = dict()
        self.scope_stack = [self]
        self.funDefSymbol = None

    def put(self, name, symbol):  # put variable symbol or fundef under <name> entry
        if self.scope_stack:
            self.getCurrentScope().symbols[name] = symbol
        else:
            self.symbols[name] = symbol

    def get(self, name):  # get variable symbol or fundef from <name> entry
        return self.symbols.get(name)

    def getParentScope(self):
        return self.parent

    def pushScope(self, name):
        #  check if we have a scope stack,
        #  if so then set parent of the created symbol table as the first on stack

        if self.scope_stack:
            scope = SymbolTable(self.scope_stack[-1], name)
        else:
            scope = SymbolTable(self, name)
        self.scope_stack.append(scope)

    def getCurrentScope(self):
        return self.scope_stack[-1]
        # not needed since scope stack is initialized with global inside
        # if self.scope_stack:
        #     return self.scope_stack[-1]
        # else:
        #     return self

    def currentScopeSearch(self, name):
        if self.scope_stack:
            return self.getCurrentScope().get(name)
        else:
            return self.get(name)

    def popScope(self):
        self.scope_stack.pop()

    def globalScopeSearch(self, name):
        current_scope = self.getCurrentScope()
        while current_scope is not None:
            res = current_scope.get(name)
            if res is not None:
                return res
            current_scope = current_scope.parent
        # for current_scope in self.scope_stack[::-1]:
        #     res = current_scope.get(name)
        #     if res is not None:
        #         return res
        return None

    def checkScopesForName(self, names):
        return self.searchScopesForName(names) is not None

    def searchScopesForName(self, names):
        # returns boolean specifying if there exists a scope with one of the given names
        currentScope = self.getCurrentScope()
        found = False
        while currentScope is not None:
            if currentScope.name in names:
                return currentScope
            currentScope = currentScope.parent
        return None
