#!/usr/bin/python
# coding=utf-8
import re
from collections import defaultdict

import AST
from SymbolTable import SymbolTable, FunDefSymbol, VariableSymbol, LabeledInstructionSymbol

# Defined operators


arith_op = ['+', '-', '/', '*']
bin_op = ['>>', '<<', '|', '||', '^', '&', '&&']
compare_op = ['==', '!=', '>', '>=', '<', '<=']
misc_op = ['%']
# Stores types of results of allowed operations
# ttype[operator][type][type] = res_type
ttype = defaultdict(
    lambda: defaultdict(
        lambda: defaultdict(lambda: None)))

# int op int returns int
for op in arith_op + bin_op + compare_op + misc_op:
    ttype[op]['int']['int'] = 'int'

# arithmetic operations
for op in arith_op:
    ttype[op]['float']['float'] = 'float'
    ttype[op]['int']['float'] = 'float'
    ttype[op]['float']['int'] = 'float'

# compare operations
for op in compare_op:
    ttype[op]['float']['float'] = 'int'
    ttype[op]['float']['int'] = 'int'
    ttype[op]['int']['float'] = 'int'
    ttype[op]['string']['string'] = 'int'

# assignment operator
ttype['=']['string']['string'] = 'string'
ttype['=']['float']['float'] = 'float'
ttype['=']['float']['int'] = 'float'
ttype['=']['int']['int'] = 'int'
ttype['=']['int']['float'] = 'int'

# string operations
ttype['*']['string']['int'] = 'string'
ttype['+']['string']['string'] = 'string'


class NodeVisitor(object):
    def visit(self, node):
        method = 'visit_' + node.__class__.__name__
        visitor = getattr(self, method, self.generic_visit)
        return visitor(node)

    def generic_visit(self, node):  # Called if no explicit visitor function exists for a node.
        if isinstance(node, list):
            for elem in node:
                self.visit(elem)
        else:
            for child in node.children:
                if isinstance(child, list):
                    for item in child:
                        if isinstance(item, AST.Node):
                            self.visit(item)
                elif isinstance(child, AST.Node):
                    self.visit(child)

                    # simpler version of generic_visit, not so general
                    # def generic_visit(self, node):
                    #    for child in node.children:
                    #        self.visit(child)


# noinspection PyPep8Naming
class TypeChecker(NodeVisitor):
    def __init__(self):
        self.symbol_table = SymbolTable(None, "global")

    def visit_Instructions(self, node):
        for instruction in node.instructions:
            self.visit(instruction)

    def visit_Args(self, node):
        for arg in node.args:
            self.visit(arg)

    def visit_Arg(self, node):
        if self.symbol_table.getCurrentScope().get(node.id) is not None:
            print "Error: Variable '{0}' already declared: line {1}".format(node.id, node.line)
        else:
            self.symbol_table.put(node.id, VariableSymbol(node.id, node.type))
        return node.type

    def visit_Const(self, node):
        # checks constant types
        if re.match(r'\"([^\\\n]|(\\.))*?\"', node.value):
            return 'string'
        elif re.match(r"\d+(\.\d*)|\.\d+", node.value):
            return 'float'
        elif re.match(r"\d+", node.value):
            return 'int'
        else:
            symbol = self.symbol_table.globalScopeSearch(node.value)
            if symbol is None:
                print "Error: Usage of undeclared variable '{0}': line {1}".format(node.value, node.line)
            else:
                if isinstance(symbol, FunDefSymbol):
                    print "Error: Function identifier '{0}' used as a variable: line {1}".format(node.value, node.line)
                    return symbol.return_type
                return symbol.type

    def visit_Variable(self, node):
        var = self.symbol_table.globalScopeSearch(node.name)

    def visit_CodeBlocks(self, node):
        for code_block in node.children:
            self.visit(code_block)

    def visit_ExpressionList(self, node):
        for expression in node.expressions:
            self.visit(expression)

    def visit_PrintInstruction(self, node):
        self.visit(node.expr_list)

    def visit_Declarations(self, node):
        for declaration in node.declarations:
            self.visit(declaration)

    def visit_Compound(self, node):
        fun_scope = self.symbol_table.getCurrentScope().name == 'fun-scope'
        if not fun_scope:
            self.symbol_table.pushScope('compound')
        self.visit(node.declarations)
        if node.instructions_opt is not None:
            self.visit(node.instructions_opt)

        if not fun_scope:
            self.symbol_table.popScope()

    def visit_Label(self, node):
        current_scope = self.symbol_table.getCurrentScope()
        loop_labels = ('repeat-loop', 'while-loop')

        if not self.symbol_table.checkScopesForName(loop_labels):
            print "Error: {0} instruction outside a loop: line {1}".format(node.label, node.line)

    def visit_ReturnInstr(self, node):
        function_scope = self.symbol_table.searchScopesForName(['fun-scope'])
        if not function_scope:
            print "Error: return instruction outside a function: line {0}".format(node.line)
            return_type = self.visit(node.expression)
        else:
            function_type = function_scope.funDefSymbol.return_type
            return_type = self.visit(node.expression)
            if return_type is None:
                return
            if ttype['='][function_type][return_type] is None:
                print "Error: Improper returned type, expected {0}, got {1}: line {2}".format(function_type,
                                                                                              return_type, node.line)
            else:
                if function_type == 'int' and return_type == 'float':
                    print "Error: Improper returned type, expected {0}, got {1}: line {2}".format(function_type,
                                                                                                  return_type,
                                                                                                  node.line)
                if function_type == 'float' and return_type == 'int':
                    print "Warning [{0}]: Function {1} declared as returning float, but statement returns int." \
                        .format(node.line, function_scope.funDefSymbol.name)
        return return_type

    def visit_RepeatInstr(self, node):
        condition_type = self.visit(node.condition)

        self.symbol_table.pushScope('repeat-loop')
        self.visit(node.instruction)
        self.symbol_table.popScope()

        if ttype['=']['int'][condition_type] is None:
            print "Error: Argument type mismatch in an while loop. Expected int, got {0}: line {1}".format(
                condition_type,
                node.line)

    def visit_WhileInstr(self, node):
        condition_type = self.visit(node.condition)
        if ttype['=']['int'][condition_type] is None:
            print "Error: Argument type mismatch in an while loop. Expected int, got {0}: line {1}".format(
                condition_type,
                node.line)

        self.symbol_table.pushScope('while-loop')
        self.visit(node.instruction)
        self.symbol_table.popScope()

    def visit_ChoiceInstr(self, node):
        condition_type = self.visit(node.condition)
        if ttype['=']['int'][condition_type] is None:
            print "Error: Argument type mismatch in an if statement. Expected int, got {0}: line {1}".format(
                condition_type,
                node.line)
        self.visit(node.instruction)

        if node.else_instruction is not None:
            self.visit(node.else_instruction)

    def visit_Assignment(self, node):
        symbol = self.symbol_table.globalScopeSearch(node.id)
        if symbol is None:
            print "Error: Variable '{0}' undefined in current scope: line {1}".format(node.id, node.line)
            self.visit(node.expression)
        elif not isinstance(symbol, VariableSymbol):
            print "Expected variable, not {0} in line {1}".format(type(symbol).__name__, node.line)
            self.visit(node.expression)
        else:
            provided = self.visit(node.expression)
            expected = symbol.type
            if ttype['='][expected][provided] is None:
                print "Error: Argument type mismatch at assignment. Expected {0} got {1}: line {2}". \
                    format(expected, provided, node.line)
            elif provided is 'float' and expected is 'int':
                print "Warning: Possible loss of precision: line {0}".format(node.line)

    def visit_LabeledInstruction(self, node):
        if self.symbol_table.globalScopeSearch(node.name) is not None:
            print "Name {0} was already declared before. Line: {1}".format(node.name, node.line)
        else:
            self.symbol_table.putInCurrentScope(LabeledInstructionSymbol(node.name, node.instruction))
            self.visit(node.instruction)

    def visit_BinExpr(self, node):
        type1 = self.visit(node.left)
        type2 = self.visit(node.right)
        op = node.op
        res_type = ttype[op][type1][type2]
        if res_type is None:
            print "Error: Illegal operation, {0} {1} {2}: line {3}".format(type1, op, type2, node.line)
        else:
            return res_type

    # wywołanie niezdefiniowanej funkcji
    # użycie w wywołaniu funkcji parametrów lub wartości zwracanej o typie niekompatybilnym z sygnaturą funkcji
    def visit_FunctionInvokation(self, node):
        # search global scope for the function name id
        fun_def = self.symbol_table.globalScopeSearch(node.id)
        if fun_def is None:
            print "Error: Call of undefined fun '{0}': line {1}".format(node.id, node.line)
        else:
            arg_count = len(node.args.expressions) if node.args is not None else 0
            if arg_count is not len(fun_def.args.args):
                print "Error: Improper number of args in {0} call: line {1}".format(node.id, node.line)
            else:
                # Check argument types
                for i, arg in enumerate(node.args.expressions):
                    expected = fun_def.args.args[i].type

                    provided = self.visit(arg)
                    # Check if types match
                    if ttype['='][expected][provided] is None:
                        print "Improper type of args in {0} call: line {1}". \
                            format(node.id, node.line)
                        break
                    else:
                        if expected is 'int' and provided is 'float':
                            print "Warning: Possible loss of precision: line {0}".format(node.line)
            return fun_def.return_type

    def visit_Declaration(self, node):
        if node.inits is None:
            print "Error: Use of undeclared variable : line {0}".format(node.line)
        for init in node.inits.inits:
            name = init.id
            # Check the type of the expression
            type = self.visit(init.expression)
            if ttype['='][node.type][type] is None:
                print "Error: Assignment of {0} to {1}: line {2}". \
                    format(type, node.type, node.line)
            else:
                # Checking if we are not naming the variable with the function identifier
                var_def = self.symbol_table.globalScopeSearch(name)
                if var_def is not None and isinstance(var_def, FunDefSymbol):
                    print "Error: Function identifier '{0}' used as a variable: line {1}".format(name, node.line)
                # Checking if variable has not been declared already
                elif self.symbol_table.getCurrentScope().get(name) is not None:
                    print "Error: Variable '{0}' already declared: line {1}".format(name, node.line)

                else:
                    if node.type is 'int' and type is 'float':
                        print "Warning: Possible loss of precision: line {0}".format(node.line)
                    # If there are no errors put the variable name into current scope
                    self.symbol_table.put(name, VariableSymbol(name, node.type))

    def get_last_instruction(self, instruction):
        if isinstance(instruction, AST.Compound):
            return self.get_last_instruction(instruction.instructions_opt.instructions[-1])
        if isinstance(instruction, AST.ChoiceInstr):
            if instruction.else_instruction:
                return self.get_last_instruction(instruction.instruction) and self.get_last_instruction(
                    instruction.else_instruction)
            else:
                return self.get_last_instruction(instruction.instruction)
        if isinstance(instruction, AST.WhileInstr):
            return self.get_last_instruction(instruction.instruction)
        if isinstance(instruction, AST.RepeatInstr):
            return self.get_last_instruction(instruction.instructions)
        if isinstance(instruction, AST.Instructions):
            return self.get_last_instruction(instruction.instructions[-1])

        if isinstance(instruction, AST.ReturnInstr):
            return True
        return False

    def visit_FunDef(self, node):
        if self.symbol_table.get(node.id) is not None:
            print "Error: Redefinition of function '{0}': line {1}".format(node.id, node.line)
        else:
            fun_symbol = FunDefSymbol(node.id, node.type, node.args)
            # Add symbol to scope
            self.symbol_table.put(node.id, fun_symbol)
            self.symbol_table.pushScope("fun-scope")
            self.symbol_table.getCurrentScope().funDefSymbol = fun_symbol
            self.visit(node.args)
            self.visit(node.instructions)
            last_instruction_is_return = self.get_last_instruction(node.instructions)
            if not last_instruction_is_return:
                print "Error: Missing return statement in function '{0}' returning {1}: line {2}".format(node.id,
                                                                                                         node.type,
                                                                                                         node.line)
            self.symbol_table.popScope()
